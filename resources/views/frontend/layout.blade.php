
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Mã giảm giá</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{asset('assets/frontend/images/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{asset('assets/frontend/images/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/frontend/images/apple-touch-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/frontend/images/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/frontend/images/apple-touch-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/frontend/images/apple-touch-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/frontend/images/apple-touch-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/frontend/images/apple-touch-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/frontend/images/apple-touch-icon-152x152.png')}}">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/style.css')}}">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/responsive.css')}}">
    <!-- COLORS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/colors.css')}}">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/custom.css')}}">
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Skin Examples -->
    <link rel="alternate stylesheet" type="text/css" href="{{asset('assets/frontend/css/skins/skin1.css')}}" media="all" />
    <!-- END Skin Examples -->

</head>
<body>
    <!-- ******************************************
    START SITE HERE
    ********************************************** -->

    <div id="wrapper">
        <div class="header">
            <div class="menu-wrapper ab-menu" style="background-image:url('{{ asset('assets/frontend/uploads/parallax_01.jpg') }}');">
                <div class="container">
                    <div class="hovermenu ttmenu menu-color">
                        <div class="navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="assets/frontend/index.html"><img src="{{asset('assets/frontend/images/logo.png')}}" alt=""></a>
                            </div><!-- end navbar-header -->
                    
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li><a class="" href="{{route('homepage')}}" title="">Trang Chủ</a></li>
                                    <!-- <li class="dropdown hasmenu">
                                        <a href="assets/frontend/#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="fa fa-angle-down"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="assets/frontend/store-single.html">Store Single</a></li>
                                            <li><a href="assets/frontend/coupon-single.html">Coupon Single</a></li>
                                            <li><a href="assets/frontend/category-single.html">Category Single</a></li>
                                            <li><a href="assets/frontend/single.html">Blog Single</a></li>
                                            <li><a href="assets/frontend/page-contact.html">Contact us</a></li>
                                            <li><a href="assets/frontend/page.html">Default Page</a></li>
                                            <li><a href="assets/frontend/page-404.html">Not Found</a></li>
                                            <li><a href="assets/frontend/page-elements.html">All Elements</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="assets/frontend/coupons.html" title="">Coupons</a></li>
                                    <li><a href="assets/frontend/printable.html" title="">Printable</a></li>
                                    <li><a href="assets/frontend/categories.html" title="">Categories</a></li>
                                    <li><a href="assets/frontend/stores.html" title="">Stores</a></li>
                                    <li><a href="assets/frontend/blog.html" title="">Blog</a></li> -->
                                </ul>
                             <!--    <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown hasmenu userpanel">
                                        <a href="assets/frontend/#" class="dropdown-toggle" data-toggle="dropdown">
                                            <img src="{{asset('assets/frontend/uploads/testi_03.png')}}" alt="" class="img-circle"> <span class="fa fa-angle-down"></span></a>
                                        <ul class="dropdown-menu start-right" role="menu">
                                            <li><a href="assets/frontend/user-dashboard.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                            <li><a href="assets/frontend/user-favorites.html"><i class="fa fa-star"></i> Favorite Stores</a></li>
                                            <li><a href="assets/frontend/user-saved.html"><i class="fa fa-heart-o"></i> Saved Coupons</a></li>
                                            <li><a href="assets/frontend/user-submit.html"><i class="fa fa-bullhorn"></i> Submit Coupon</a></li>
                                            <li><a href="assets/frontend/#"><i class="fa fa-lock"></i> Sign Out</a></li>
                                        </ul>
                                    </li>
                                </ul> -->
                            </div><!--/.nav-collapse -->
                        </div><!-- end navbar navbar-default clearfix -->
                    </div><!-- end menu 1 --> 
                </div><!-- end container --> 
            </div><!-- / menu-wrapper -->
        </div><!-- end header -->

        @yield('body')
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Email Newsletter</h4>
                            </div>

                            <div class="newsletter">
                                <p>Your email is safe with us and we hate spam as much as you do. Lorem ipsum dolor sit amet et dolore.</p>
                                <form class="">
                                    <input type="text" class="form-control" placeholder="Enter your name..">
                                    <input type="email" class="form-control" placeholder="Enter your email..">
                                    <button type="submit" class="btn btn-primary">SUBSCRIBE</button>
                                </form>
                            </div>
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Twitter Feed's</h4>
                            </div>
                            <div class="twitter-widget">
                                <ul class="twitter_feed">
                                    <li><span></span><p>Envato Market wishes you and your family a merry Christmas and a happy new! You are awesome! <a href="assets/frontend/#">about 2 days ago</a></p></li>
                                    <li><span></span><p>PSD Convert HTML, thanks for support and a merry Christmas and a happy new years! <a href="assets/frontend/#">about 9 days ago</a></p></li>
                                </ul><!-- end twiteer_feed --> 
                            </div>
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-2 col-sm-12 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Company Info</h4>
                            </div>

                            <ul class="footer-links">
                                <li><a href="assets/frontend/#">About us</a></li>
                                <li><a href="assets/frontend/#">Contact us</a></li>
                                <li><a href="assets/frontend/#">Terms of Usage</a></li>
                                <li><a href="assets/frontend/#">Copyrights</a></li>
                                <li><a href="assets/frontend/#">Coupon a Report</a></li>
                                <li><a href="assets/frontend/#">License</a></li>
                                <li><a href="assets/frontend/#">Trademarks</a></li>
                            </ul><!-- end links -->
                        </div><!-- end widget -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Frequently Asked Questions</h4>
                            </div>

                            <div class="first_accordion withicon withcolorful panel-group" id="accordion5">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion5" href="assets/frontend/#collapse115">
                                                <i class="fa fa-angle-down"></i> How to Use Coupon Codes?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse115" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiolore. consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolor.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion5" href="assets/frontend/#collapse215">
                                                <i class="fa fa-angle-down"></i> Can I submit My Coupons?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse215" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiolore. consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolor.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-primary last">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion5" href="assets/frontend/#collapse315">
                                                <i class="fa fa-angle-down"></i> Coupon Submission free?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse315" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiolore. consectetur adipisicing elit, sed do eiusmod tempor ut labore et dolor.</p>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end panel -->
                        </div><!-- end widget -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer -->

        <div class="copyrights">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="copylinks">
                            <ul class="list-inline">
                                <li><a href="assets/frontend/#">Home</a></li>
                                <li><a href="assets/frontend/#">About</a></li>
                                <li><a href="assets/frontend/#">Contact</a></li>
                                <li><a href="assets/frontend/#">Terms of Usage</a></li>
                                <li><a href="assets/frontend/#">Trademark</a></li>
                                <li><a href="assets/frontend/#">License</a></li>
                            </ul><!-- end ul -->
                            <p>magiamgia.club &copy; 2020 - Designed by <a href="assets/frontend/http://psdconverthtml.com/">PSD to HTML</a></p>
                        </div><!-- end links -->
                    </div><!-- end col -->

                    <div class="col-md-6 col-sm-6">
                        <div class="footer-social text-right">
                            <ul class="list-inline social-small">
                                <li><a href="assets/frontend/#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="assets/frontend/#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="assets/frontend/#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="assets/frontend/#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="assets/frontend/#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="assets/frontend/#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
    </div><!-- end wrapper -->

    <!-- ******************************************
    /END SITE
    ********************************************** -->
    <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
    <script src="{{asset('assets/frontend/js/all.js')}}"></script>
    <script src="{{asset('assets/frontend/js/custom.js')}}"></script>
    <script src="{{asset('assets/frontend/js/common.js')}}"></script>
</body>
</html>