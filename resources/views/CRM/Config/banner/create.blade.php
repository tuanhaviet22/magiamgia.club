@extends('CRM.layout')
@push('myscript')
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script src="{{ asset('assets/crm/js/common.js') }}"></script>
@endpush
@section('content')
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
   <!-- begin:: Subheader -->
   <div class="kt-subheader   kt-grid__item" id="kt_subheader">
      <div class="kt-subheader__main">
         <h3 class="kt-subheader__title">
            Thêm banner
         </h3>
         <span class="kt-subheader__separator kt-hidden"></span>
         <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{route('banner.index')}}" class="kt-subheader__breadcrumbs-link">
            Danh sách </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="{{ route('banner.create') }}" class="kt-subheader__breadcrumbs-link">
            Thêm </a>
            <span class="kt-subheader__breadcrumbs-separator"></span>           
         </div>
      </div>
   </div>
   <!-- end:: Subheader -->
   <!-- begin:: Content -->
   <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
               {{--  
               <div class="kt-portlet__head">
                  <div class="kt-portlet__head-label">
                     <h3 class="kt-portlet__head-title">
                        Basic Form Layout
                     </h3>
                  </div>
               </div>
               --}}
               <!--begin::Form-->
               <form class="kt-form" method="POST" action="{{route('post.banner.create')}}">
                  @csrf                  
                  <div class="kt-portlet__body">
                     <div class="kt-section kt-section--first">
                          {{-- Preview --}}
                           <div id="holder" class="text-center preview-image">
                           </div>
                           {{-- End preview --}}
                        <div class="input-group mb-3 select-banner">
                           <span class="input-group-btn">
                              <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                 <i class="fa fa-picture-o"></i> Chọn ảnh
                              </a>
                           </span>
                           <input id="thumbnail" class="form-control" type="text" name="src">
                        </div>
                        <div class="form-group">
                           <label>Đường dẫn</label>
                           <input type="text" name="url" class="form-control" placeholder="Đường dẫn banner">                          
                        </div>
                        <div class="form-group">
                           <label for="alt">Alt</label>
                           <input type="text" id="alt" name="alt" class="form-control" placeholder="Alt">                          
                        </div>
                        <div class="form-group">
                           <label for="title">Title</label>
                           <input type="text" id="title" name="title" class="form-control" placeholder="Title">                          
                        </div>                                       
                        <div class="form-group">
                           <label for="type">Kiểu banner</label>
                           <select class="form-control" name="type" id="type">
                              <option value="1">Banner trang chủ</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="kt-portlet__foot">
                     <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Tạo</button>                        
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->         
         </div>
      </div>
   </div>
   <!-- end:: Content -->
</div>
@endsection