$(document).ready(function(){
	var big_banner = new Swiper ('.big-banner-slide', {
    // Optional parameters 
    loop: true,

    // If we need pagination
    pagination: {
      el: '.big-banner-pagination',
    },

    // Navigation arrows
    navigation: {
      nextEl: '.n-right',
      prevEl: '.n-left',
    },
  });
})