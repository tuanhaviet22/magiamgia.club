<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;

class banner extends Model
{    
	protected $fillable = ['id','src','url','alt','title','type','status','created_at','updated_at'];

    protected $table = 'config_banner';

    protected $primaryKey = 'id';
    
    protected $dateFormat = 'U';
}
