<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index")->name('homepage');
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
});
Auth::routes();
Route::group(['prefix' => 'crm', 'middleware' => "auth" , 'namespace' => 'crm'], function() {
	Route::get('/', "DashboardController@index")->name('admin');

	Route::group(['prefix' => 'config', 'namespace' => 'Config'], function() {
	    Route::get('/banner', 'BannerController@index')->name('banner.index');
	    Route::get('/banner/add', 'BannerController@create')->name('banner.create');
	    Route::post('/banner/add', 'BannerController@store')->name('post.banner.create');
	});
});

